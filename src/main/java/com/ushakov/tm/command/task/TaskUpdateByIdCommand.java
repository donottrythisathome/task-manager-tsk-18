package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "update-task-by-id";
    }

    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER NAME");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String taskDescription = TerminalUtil.nextLine();
        if (serviceLocator.getTaskService().updateTaskById(taskId, taskName, taskDescription) == null) {
            throw new TaskNotFoundException();
        }
    }

}
