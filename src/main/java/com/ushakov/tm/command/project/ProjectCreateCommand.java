package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String projectDescription = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().add(projectName, projectDescription);
        if (project == null) throw new ProjectNotFoundException();
    }

}
