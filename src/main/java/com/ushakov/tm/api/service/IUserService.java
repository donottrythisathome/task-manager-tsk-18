package com.ushakov.tm.api.service;

import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.model.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User add(final String login, final String password);

    User add(final String login, final String password, final String email);

    User add(final String login, final String password, final Role role);

    User findById(final String id);

    User removeUser(final User user);

    User findByLogin(final String login);

    User removeById(final String id);

    User removeByLogin(final String login);

    User setPassword(final String userId, final String password);

    User updateUser(final String userId, final String firstName, final String lastName, final String middleName);

}
