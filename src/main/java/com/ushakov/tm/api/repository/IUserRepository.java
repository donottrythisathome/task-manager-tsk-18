package com.ushakov.tm.api.repository;

import com.ushakov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(final User user);

    User findById(final String id);

    User removeUser(final User user);

    User findByLogin(final String login);

    User findByEmail(final String email);

    User removeById(final String id);

    User removeByLogin(final String login);

}
