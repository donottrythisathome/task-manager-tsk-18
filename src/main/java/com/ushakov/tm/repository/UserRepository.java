package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.IUserRepository;
import com.ushakov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> list = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return list;
    }

    @Override
    public User add(final User user) {
        list.add(user);
        return user;
    }

    @Override
    public User findById(final String id) {
        for (final User user: list) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User removeUser(final User user) {
        list.remove(user);
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user: list) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeById(final String id) {
        final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public User findByEmail(String email) {
        for (final User user: list) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

}
