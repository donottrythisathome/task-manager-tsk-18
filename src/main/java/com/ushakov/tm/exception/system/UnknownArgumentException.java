package com.ushakov.tm.exception.system;

import com.ushakov.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(String value) {
        super("Error! Unknown argument: " + value + "!");
    }

}
